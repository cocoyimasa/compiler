﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Compiler;
using System.Collections.Generic;
using Compiler.Ast;

namespace CompilerUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestIntAndFloat()//passed
        {
            string code = "let a=10;\n" +
                "let b :int=-2;\n" +
                "let c : float= 10000.23567;\n" +
                "let d = 136644;\n" +
                "let e = -193746.534;\n" +
                "let f = 3424.213;\n";
            LexicalAnalyzer.InitTokenRules();
            List<Token> tokens = LexicalAnalyzer.Tokenizer(code);
            SyntacticalAnalyzer parser = new SyntacticalAnalyzer();
            parser.tokens = tokens;
            parser.ParseProgram();
        }
        [TestMethod]
        public void TestStringAndBool()//passed
        {
            string code = "let a=\"sgdhhehe\";" +
                "let b :string=\"ddedw\";\n" +
                "let c : bool= true;\n" +
                "let d = false;\n" +
                "let e :bool= 1;\n" +
                "let f :string= \"true\";\n";
            LexicalAnalyzer.InitTokenRules();
            List<Token> tokens = LexicalAnalyzer.Tokenizer(code);
            SyntacticalAnalyzer parser = new SyntacticalAnalyzer();
            parser.tokens = tokens;
            parser.ParseProgram();
        }
        [TestMethod]
        public void TestVar()//passed
        {
            string code = "let a=1;" +
                "let b =a+1;\n" +
                "let c : bool= true;\n" +
                "let d = c;\n" +
                "var e :bool= 132;\n" +
                "var f :string= 12322;\n";
            LexicalAnalyzer.InitTokenRules();
            List<Token> tokens = LexicalAnalyzer.Tokenizer(code);
            SyntacticalAnalyzer parser = new SyntacticalAnalyzer();
            parser.tokens = tokens;
            parser.ParseProgram();
        }
        [TestMethod]
        public void TestImport()//passed
        {
            string code = "import * from Package1;" 
                +"import * from Package2;" 
                +"import {A} from Package;"
                +"import {A,B} from Package;" 
                +"import {A,B,c} from Package;"  ;
            LexicalAnalyzer.InitTokenRules();
            List<Token> tokens = LexicalAnalyzer.Tokenizer(code);
            SyntacticalAnalyzer parser = new SyntacticalAnalyzer();
            parser.tokens = tokens;
            parser.ParseProgram();
        }
        [TestMethod]
        public void TestModifier()//passed
        {
            string code = "static var a = 93764;" +
                "const var b = 36632;" +
                "const var c:string = \"dfeerw\";" +
                "static var m:float=123.232;" +
                "static X x = new X();";
            LexicalAnalyzer.InitTokenRules();
            List<Token> tokens = LexicalAnalyzer.Tokenizer(code);
            SyntacticalAnalyzer parser = new SyntacticalAnalyzer();
            parser.tokens = tokens;
            parser.ParseProgram();
        }
        [TestMethod]
        public void TestFunction()//passed
        {
            string code = "function a(){}\r\n" +
                "function cdhdhdg233s(a,b)\r\n{var c;}\r\n" +
                "function dhyeue37rhtsgf(a:int,b:string)\r\n{\r\n    var d=1;\r\n}\r\n" +
                "function dgg(e:float,Mdhdhe:UserDefined):int{\r\n return 10;\r\n}\r\n" +
                "function ddh(dhd:UserDefined,User:User):User\r\n{\r\n \t let n;}\r\n";
            LexicalAnalyzer.InitTokenRules();
            List<Token> tokens = LexicalAnalyzer.Tokenizer(code);
            SyntacticalAnalyzer parser = new SyntacticalAnalyzer();
            parser.tokens = tokens;
            Node program = parser.ParseProgram();
            Assert.IsNotNull(program);
        }
        [TestMethod]
        public void TestFuncCall()//passed
        {
            string code = "dgdg();\r\n" +
                "dggerrs(a,10);\r\n" +
                "dteyxvrwjf(dhfhe,dg,293.0,62662.352362,\"2663\");\r\n"
                ;
            LexicalAnalyzer.InitTokenRules();
            List<Token> tokens = LexicalAnalyzer.Tokenizer(code);
            SyntacticalAnalyzer parser = new SyntacticalAnalyzer();
            parser.tokens = tokens;
            Node program = parser.ParseProgram();
            Assert.IsNotNull(program);
        }
        [TestMethod]
        public void TestLambdaExpression()
        {
            string code = "hhfg(lambda(){})" +
                "dje(lambda(s,d){})" +
                "dhe(lambda(d:int){})" +
                "lambda(i,c,d){c=63.02773;}(0,1,2);";
            LexicalAnalyzer.InitTokenRules();
            List<Token> tokens = LexicalAnalyzer.Tokenizer(code);
            SyntacticalAnalyzer parser = new SyntacticalAnalyzer();
            parser.tokens = tokens;
            //Node program = parser.ParseProgram();
            //Assert.IsNotNull(program);
        }
        [TestMethod]
        public void TestClassAndInterface()
        {
            string code = "class Abdjjf{}\r\n" +
                "class Cgdg:Bghdhg{}\r\n" +
                "class Tdfd:hdh,de\r\n{\r\nvar @s;\r\n var @dedf;\r\n var dff;\r\n}\r\n" +
                "class M:C \r\n {\r\nfunction dh(){}\r\n}\r\n" +
                "interface dhh{}" +
                "interface DFdgf:Gtd{function dhdg(){}}";
            LexicalAnalyzer.InitTokenRules();
            List<Token> tokens = LexicalAnalyzer.Tokenizer(code);
            SyntacticalAnalyzer parser = new SyntacticalAnalyzer();
            parser.tokens = tokens;
            //Node program = parser.ParseProgram();
            //Assert.IsNotNull(program);
        }
        [TestMethod]
        public void TestInteface()//passed
        {
            string code = "interface Test{" +
                        "    function test();" +
                        "    function test2();}";
            LexicalAnalyzer.InitTokenRules();
            List<Token> tokens = LexicalAnalyzer.Tokenizer(code);
            SyntacticalAnalyzer parser = new SyntacticalAnalyzer();
            parser.tokens = tokens;
            //Node program = parser.ParseProgram();
            //Interpreter t = new Interpreter(program);
            //Scope s = Scope.initScope();
            //t.Interpret(s);
        }
        [TestMethod]
        public void TestIfElseWhileReturnBreakContinue()
        {
            string code = "if true {}";
            LexicalAnalyzer.InitTokenRules();
            List<Token> tokens = LexicalAnalyzer.Tokenizer(code);
            SyntacticalAnalyzer parser = new SyntacticalAnalyzer();
            parser.tokens = tokens;
            //Node program = parser.ParseProgram();
            //Assert.IsNotNull(program);
        }
        [TestMethod]
        public void TestArrayTupleDict()
        {
            string code = "var arr : int= [1,23,4,23,42,42,3,2,32];" +
                "var c = {\"ss\":1,\"sdhg\":2,c:3};" +
                "var d = (1,2,3,4,5,6,7,8,9);";
            LexicalAnalyzer.InitTokenRules();
            List<Token> tokens = LexicalAnalyzer.Tokenizer(code);
            SyntacticalAnalyzer parser = new SyntacticalAnalyzer();
            parser.tokens = tokens;
            //Node program = parser.ParseProgram();
            //Assert.IsNotNull(program);
        }
        [TestMethod]
        public void TestMathAndLogicExpression()
        {
            string code = "static var a = 93764;" +
                "const var b = 36632;" +
                "const var c:string = \"dfeerw\";" +
                "static var m:float=123.232;" +
                "static X x = new X();";
            LexicalAnalyzer.InitTokenRules();
            List<Token> tokens = LexicalAnalyzer.Tokenizer(code);
            SyntacticalAnalyzer parser = new SyntacticalAnalyzer();
            parser.tokens = tokens;
            //parser.ParseProgram();
        }
        [TestMethod]
        public void TestPrimaryExpression()
        {
            string code = "2>1" +
                "1+2 == 3" +
                "true == true" +
                "true != false" +
                "1!=2" +
                "1==1 && c=10" +
                "1==1 || 2==1" +
                "100/2*2-2+2-40+40+(100-100)+(100+(-100))";
            //LexicalAnalyzer.InitTokenRules();
            //List<Token> tokens = LexicalAnalyzer.Tokenizer(code);
            //SyntacticalAnalyzer parser = new SyntacticalAnalyzer();
            //parser.tokens = tokens;
            //Node program = parser.ParseProgram();
            //Assert.IsNotNull(program);
        }
        //[TestMethod]
        //public void TestMethod1()//passed
        //{
        //    string code = "let a = [1,2,4];";
        //    string code = "function add(a,b){return 10;}";
        //    LexicalAnalyzer.InitTokenRules();
        //    List<Token> tokens = LexicalAnalyzer.Tokenizer(code);
        //    SyntacticalAnalyzer parser = new SyntacticalAnalyzer();
        //    parser.tokens = tokens;
        //    parser.ParseProgram();
        //    Assert.AreEqual(tokens[0].Value, "let");
        //    Assert.AreEqual(tokens[1].Value, "a");
        //    Assert.AreEqual(tokens[2].Value, "=");
        //    Assert.AreEqual(tokens[3].Value, "[");
        //}
        [TestMethod]
        public void TestInterpret()//passed
        {
            string code = "let a = [1,2,4];";
            //string code = "function add(a,b){return 10;}";
            LexicalAnalyzer.InitTokenRules();
            List<Token> tokens = LexicalAnalyzer.Tokenizer(code);
            SyntacticalAnalyzer parser = new SyntacticalAnalyzer();
            parser.tokens = tokens;
            //Node program = parser.ParseProgram();
            //Assert.IsNotNull(program);
        }

        
        //[TestMethod]
        //public void TestImport()//passed
        //{
        //    string code = "import {A,B} from package;" +
        //        "import {C,D} from package1;";
        //    LexicalAnalyzer.InitTokenRules();
        //    List<Token> tokens = LexicalAnalyzer.Tokenizer(code);
        //    SyntacticalAnalyzer parser = new SyntacticalAnalyzer();
        //    parser.tokens = tokens;
        //    Node program = parser.ParseProgram();
        //    Assert.IsNotNull(program);
        //}
        [TestMethod]
        public void TestTypeAlias()//passed
        {
            string code = "typedef A B;" +
                "typedef C D;";
            LexicalAnalyzer.InitTokenRules();
            List<Token> tokens = LexicalAnalyzer.Tokenizer(code);
            SyntacticalAnalyzer parser = new SyntacticalAnalyzer();
            parser.tokens = tokens;
            //Node program = parser.ParseProgram();
            //Assert.IsNotNull(program);
        }
        [TestMethod]
        public void TestVariableAndContant()//passed
        {
            //string code = "let a33dsdafsdf =10;" +
            //    "var bcc233 = [1,2];";
            string code = 
                "var d:double = 10.01;\r\n"+
                "var c:int = -122232343;\r\n" +
                "var eddfh38yeeh4sh : bool = true;\r\n" +
                "let edhryryt: string =\"dggfgdgfgyy74737266473362773727623\" ;\r\n";
            LexicalAnalyzer.InitTokenRules();
            List<Token> tokens = LexicalAnalyzer.Tokenizer(code);
            SyntacticalAnalyzer parser = new SyntacticalAnalyzer();
            parser.tokens = tokens;
            //Node program = parser.ParseProgram();
            //Assert.IsNotNull(program);
        } 
    }
}
