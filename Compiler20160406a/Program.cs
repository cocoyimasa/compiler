﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Compiler.Ast;
using Compiler.CodeGen;

namespace Compiler
{
    static class EasyPy
    {
        public static List<TypeSystem.AbstractType>
            intArgs = new List<TypeSystem.AbstractType>() { 
                    TypeSystem.intType, 
                    TypeSystem.intType, 
                    TypeSystem.intType 
                };
        public static List<TypeSystem.AbstractType>
            boolArg = new List<TypeSystem.AbstractType>() {
                TypeSystem.boolType, 
                TypeSystem.boolType 
                };
        public static List<TypeSystem.AbstractType>
            boolArgs = new List<TypeSystem.AbstractType>() { 
                    TypeSystem.boolType, 
                    TypeSystem.boolType, 
                    TypeSystem.boolType 
                };

        public static Dictionary<Identifier, TypeSystem.AbstractType> _env =
            new Dictionary<Identifier, TypeSystem.AbstractType>();
        public static void InitEnv()
        {
            Identifier[] basicMathOps = 
                {
                    Identifier.NewIdentifier("+"  ),
                    Identifier.NewIdentifier("-"  ),
                    Identifier.NewIdentifier("*"  ),
                    Identifier.NewIdentifier("/"  )
                };
            Identifier[] logicOps = 
                {
                    Identifier.NewIdentifier("<"  ),
                    Identifier.NewIdentifier("<=" ),
                    Identifier.NewIdentifier(">"  ),
                    Identifier.NewIdentifier(">=" ),
                    Identifier.NewIdentifier("="  ),
                    Identifier.NewIdentifier("and"),
                    Identifier.NewIdentifier("or" ),
                    Identifier.NewIdentifier("not"),
                    Identifier.NewIdentifier("<>" )
                };
            TypeSystem.TypeOperator operatorInt = new TypeSystem.TypeOperator("->", intArgs);
            TypeSystem.TypeOperator operatorBool = new TypeSystem.TypeOperator("->", boolArgs);
            for (int i = 0; i < basicMathOps.Length; i++)
            {
                _env.Add(basicMathOps[i], operatorInt);
            }
            for (int i = 0; i < logicOps.Length; i++)
            {
                _env.Add(logicOps[i], operatorBool);
            }

        }
        public static void SapphireConsole(Scope env)
        {
            InitEnv();
            Scope envType = Scope.initTypeScope();
            while (true)
            {
                try
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write(">");
                    StringBuilder sb = new StringBuilder();
                    string input = Console.ReadLine();
                    while (true)
                    {
                        sb.Append(input);
                        ConsoleKeyInfo info = Console.ReadKey();
                        if (info.KeyChar == '\r')
                        {
                            break;
                        }
                        input = info.KeyChar + Console.ReadLine();
                    }
                    input = sb.ToString();
                    if (input == "!q")
                    {
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        Console.WriteLine("!...");
                        break;
                    }
                    if (!string.IsNullOrWhiteSpace(input))
                    {
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        LexicalAnalyzer.InitTokenRules();

                        SyntacticalAnalyzer analyzer = new SyntacticalAnalyzer();
                        List<Token> tokens = LexicalAnalyzer.Tokenizer(input);
                        analyzer.tokens = tokens;
                        Node result = analyzer.ParseProgram();
                        //if (result != null)
                        //{
                        //    Console.WriteLine("Compiling...Syntactical Analysis Completed!");
                        //}
                        TypeChecker tc = new TypeChecker();
                        TypeChecker.self = tc;

                        #region TypeCheck
                        Value value = tc.TypeCheck(result, envType);
                        Console.WriteLine(value == null ? "" : value.Type());
                        Interpreter interpret = new Interpreter(result);
                        Value val = interpret.Interpret(env);
                        Console.WriteLine(val.ToString());
                        #endregion

                        #region TypeSystem
                        TypeSystem ts = new TypeSystem();
                        TypeSystem.AbstractType res = ts.Analyze(result, _env);
                        Console.WriteLine(res.ToString());
                        #endregion

                        #region CodeGenerator
                        CodeGenerator gene = new CodeGenerator();
                        result.Generate(gene);
                        Console.WriteLine(gene.codes.Count);
                        foreach (var code in gene.codes)
                        {
                            Console.WriteLine(code);
                        }
                        #endregion
                    }
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(e.Message);
                }
            }
        }
    }
    class Program
    {
        
        static void Main(string[] args)
        {
            //string code = "let a = [1,2,4];";//passed
            //string code = "function add(a,b){let a=10;}";//passed
            //string code = "function add(a:int,b:int):int{var a;"
            //    +"var c=10000;"
            //    +"return c;"
            //    +"}";//passed
            //string code = "var code =\"100000\";";//passed!
            string code = "var c = 1e10;";//failed
            LexicalAnalyzer.InitTokenRules();
            List<Token> tokens = LexicalAnalyzer.Tokenizer(code);
            SyntacticalAnalyzer parser = new SyntacticalAnalyzer();
            parser.tokens = tokens;
            Node res = parser.ParseProgram();
            Console.WriteLine(res);
            Console.ReadKey();
        }
    }
}
/*#Old features
     * (1)if while:(条件不用括号括起来)
     * if a>b and a>c {}
     * while a>b and a>c {}
     * (2)lambda:
     * lambda x:int,y:int->int{let a=(a+b);}
     * (3)let:
     * let [a,b,c]=[1,2,43];
     * let a=10;
     * let c=[1,3,4,5,6,6];//Array
     * (3)函数:
     * function func(a,b,c){}
     * function func(a:int,b:float,c:string,d:UserType):UserType{}
     * (4)类:
     * class Name
     * {
     * //名字需要在语法分析时转换为ClassName_Method ClassName_field形式
     *      var a:int;var b:int;
     *      function func(a,b,c){}
     *      function Name(){}//:ctor
     * }
     * (5)类实例化和方法调用,字段使用
     * let a=new ClassName(a,b,c);
     * let a.a=10;//
     * let abc=a.a;
     * let abc=a.fun(1,2,3);
     * a.func(1,2,4);//
     * 
     */
/*#new features:
 * 
 * ##keywords:
 * 1.change def to function       √
 * 
 * ##grammar:
 * 1.Inheritance : :              √
 * 2.Interface :interface         √
 * 3.Templates :<>
 * 4.virtual functions : virtual? √
 * 5.type cast
 * 6.intanceof or is
 * 7.access control
 * 8.bit operators
 * 9.pattern matching
 * 10.nested function
 * 11.operator overload
 * 12.const (const is not let!!!swift is wrong!)
 * 13.static
 * 14.yield
 * 15.macros
 * 
 * ##Unicode Support!!!
 * 
 * ##Standard Library
 * 1.threads and processes
 * 2.networks
 * 3.collections
 * 4.streams
 * 5.native function call
 * 6.
 * 
 * ##create a website for my language!!!
 */
