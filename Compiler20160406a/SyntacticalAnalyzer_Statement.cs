﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Compiler.Ast;
using System.Diagnostics;

namespace Compiler
{
    public partial class SyntacticalAnalyzer
    {
        /*
         * statement → expression
         * statement → declaration
         * 
         * statement -> class-statement
         * class-statement -> keyword-class class-decl class-body
         * interface-statement -> keyword-interface class-decl interface-body
         * statement -> interface-statement
         * statement -> func-call-statement  
         * func-call-statement -> func-call-exp ;
         * 
           statement → loop-statement
           statement → branch-statement -- if switch
           statement → control-transfer-statement
           **statement → do-statement
           statements → statement statements
         * **/
        public List<Node> ParseStatementList()
        {
            List<Node> statementList = new List<Node>();
            while (true)
            {
                if (ReadToken(TokenType.FUNCTION))
                {
                    statementList.Add(ParseFunction());
                }
                else if (ReadToken(TokenType.CLASS))
                {
                    statementList.Add(ParseClass());
                }
                else if (ReadToken(TokenType.LET))
                {
                    statementList.Add(ParseVariable());
                }
                else if (ReadToken(TokenType.VAR))
                {
                    statementList.Add(ParseVariable());
                }
                else if (ReadToken(TokenType.IF))
                {
                    statementList.Add(ParseIfElseStatement());
                }
                else if (ReadToken(TokenType.WHILE))
                {
                    statementList.Add(ParseWhile());
                }
                else if (ReadToken(TokenType.RETURN))
                {
                    var st = ParseControlTransferStatement(ControlTransferStatement.ControlTransferType.Return_Statement);
                    statementList.Add(st);
                }
                else if (ReadToken(TokenType.BREAK))
                {
                    var st = ParseControlTransferStatement(ControlTransferStatement.ControlTransferType.Break_Statement);
                    statementList.Add(st);
                }
                else if (ReadToken(TokenType.CONTINUE))
                {
                    var st = ParseControlTransferStatement(ControlTransferStatement.ControlTransferType.Continue_Statement);
                    statementList.Add(st);
                }
                else if (LookToken(TokenType.Identifier) && LookToken("(", 1))
                {
                    bool R = ReadToken(TokenType.Identifier);
                    Debug.Assert(R);
                    statementList.Add(ParseFuncCallStatement());
                    //or class method call...
                }
                else if (LookToken(TokenType.Identifier) && LookToken(".", 1))
                {
                    bool R = ReadToken(TokenType.Identifier);
                    Debug.Assert(R);
                    statementList.Add(ParseMethodCall());
                }
                else
                {
                    break;
                }
            }
            return statementList;
        }
        //else {statements}
        public Node ParseElseStatement()
        {
            Block elseBody = null;
            bool res = ReadToken("{");
            Debug.Assert(res);
            elseBody = (Block)ParseBlock();
            res = ReadToken("}");
            Debug.Assert(res);

            return elseBody;
        }
        //IfSt -> if cond {statements} 
        public Node ParseIfStatement()
        {
            Node test = null;
            Block thenBody = null;
            int leftIndex = 0;
            if (FindToken(ref leftIndex, "{"))
            {
                test = ParseExpression(leftIndex);
            }
            else
            {
                throw new CodeException(token, "缺少{");
            }
            bool res = ReadToken("{");
            Debug.Assert(res);
            thenBody = (Block)ParseBlock();
            res = ReadToken("}");
            Debug.Assert(res);
            return new IfStatement(Token.NewToken("statement"), test, thenBody);
        }
        // ifSt [else if IfSt]* [elseSt]
        public Node ParseIfElseStatement()
        {
            List<Node> elseIfStatements = new List<Node>(); 
            Block elseBody = new Block(Token.NewToken("nullElse"), new List<Node>());

            IfStatement st = (IfStatement)ParseIfStatement();

            while (ReadToken(TokenType.ELSE))
            {
                if(ReadToken(TokenType.IF))
                {
                    elseIfStatements.Add(ParseIfStatement());
                }
                else 
                { 
                    elseBody = (Block)ParseElseStatement();
                    break;
                }
            }
            st.elseIfStatements = elseIfStatements;
            st.elseBody = elseBody;

            return st;
        }
        public Node ParseWhile()
        {
            Node test = null;
            Block body = null;

            int leftIndex = 0;
            if (FindToken(ref leftIndex, "{"))
            {
                test = ParseExpression(leftIndex);
            }
            else
            {
                throw new CodeException(token, "缺少{");
            }
            if (ReadToken("{"))
            {
                body = (Block)ParseBlock();
                if (!ReadToken("}"))
                {
                    throw new CodeException(token, "缺少}");
                }
            }
            else
            {
                body = new Block(Token.NewToken("nullBody"), new List<Node>());
            }
            return new WhileStatement(Token.NewToken("statement"), test, body);
        }
        public Node ParseControlTransferStatement(ControlTransferStatement.ControlTransferType type)
        {
            Token tok;
            ControlTransferStatement st = null;
            switch (type)
            {
                case ControlTransferStatement.ControlTransferType.Return_Statement:
                    {
                        tok = Token.NewToken("return");
                        int semiIndex = 0;
                        if (FindToken(ref semiIndex, ";"))
                        {
                            var exp = ParseExpression(semiIndex);

                            st = new ReturnStatement(tok, exp);//return a ReturnStatement for code compatibility 
                        }
                    }
                    break;
                case ControlTransferStatement.ControlTransferType.Continue_Statement:
                    tok = Token.NewToken("continue");
                    st = new ControlTransferStatement(tok, type, null);
                    break;
                case ControlTransferStatement.ControlTransferType.Break_Statement:
                    tok = Token.NewToken("break");
                    st = new ControlTransferStatement(tok, type, null);
                    break;
                default:
                    tok = Token.NewToken("node");
                    break;
            }
            if (!ReadToken(";"))
            {
                throw new CodeException(token, "缺少;");
            }
            return st;
        }
        /*
         * interface InterfaceName[:interface,interfaces*]
         * {
         * function name([])[];
         * }
         */
        public Node ParseInterface()
        {
            Token firstToken = null;
            string className = "";
            List<Declaration> decls = null;
            List<VirtualFunctionDeclaration> functions = new List<VirtualFunctionDeclaration>();
            bool newScope = true;
            Scope env = null;

            CheckFirstToken(ref firstToken, ref className, "interface name not defined [ 接口名未定义 ].");

            if (!ReadToken("{"))
            {
                throw new CodeException(token, "lack of {。[ 缺少{ ].");
            }
            else
            {
                while (!ReadToken("}"))
                {
                    if (ReadToken(TokenType.FUNCTION))
                    {
                        if (newScope)
                        {
                            env = new Scope();
                            newScope = false;
                        }
                        VirtualFunctionDeclaration virtualFunc = (VirtualFunctionDeclaration)ParseVirtualFunction();
                        virtualFunc.funcName = className + "::" + virtualFunc.funcName;
                        virtualFunc.token.Value = className + "::" + virtualFunc.token.Value;
                        functions.Add(virtualFunc);
                        env.Put(virtualFunc.funcName, "type", virtualFunc);
                    }
                    else
                    {
                        throw new CodeException(token, "interface中不允许有除function以外的声明");
                    }
                }
            }
            return new InterfaceStatement(firstToken, className, functions, env);
        }
        /*
         * class ClassName [: ParentClass(Interface),
         * ParentClasses(Interfaces)*]
         * {
         *     var name[:BasicType];
         *     var name2[:CompoundType];
         *     function ClassName([]){} //ctor
         *     function func([args,args*])[:type]{
         *     //body
         *     }
         * }
         */
        public Node ParseClass()
        {
            Token firstToken = null;
            string className = "";
            List<Declaration> decls = new List<Declaration>();
            List<FunctionStatement> pubFuncs = new List<FunctionStatement>();
            List<FunctionStatement> priFuncs = new List<FunctionStatement>();
            bool newScope = true;
            Scope env = null;

            CheckFirstToken(ref firstToken, ref className, "class name not defined [ 类名未定义 ].");

            if (!ReadToken("{"))
            {
                throw new CodeException(token, "缺少{");
            }
            else
            {
                //bug
                while (!ReadToken("}"))
                {
                    if (ReadToken(TokenType.VAR))
                    {
                        if (newScope)
                        {
                            env = new Scope();
                            newScope = false;
                        }
                        Declaration decl = (Declaration)ParseVar(className);
                        decls.Add(decl);
                        env.Put(decl.name, "type", decl.type);
                    }
                    else if (ReadToken(TokenType.FUNCTION))
                    {
                        if (newScope)
                        {
                            env = new Scope();
                            newScope = false;
                        }
                        FunctionStatement fun = (FunctionStatement)ParseFunction();
                        fun.funcName = className + "::" + fun.funcName;
                        fun.token.Value = className + "::" + fun.token.Value;
                        pubFuncs.Add(fun);
                        env.Put(fun.funcName, "type", fun);
                    }
                    else if (ReadToken("@"))
                    {
                        if (newScope)
                        {
                            env = new Scope();
                            newScope = false;
                        }
                        FunctionStatement fun = (FunctionStatement)ParseFunction();
                        fun.funcName = className + "::" + fun.funcName;
                        fun.token.Value = className + "::" + fun.token.Value;
                        priFuncs.Add(fun);
                        env.Put(fun.funcName, "type", fun);
                    }
                    else
                    {
                        throw new CodeException(token, "class中不允许有除function,var以外的声明");
                    }
                }
            }
            return new ClassStatement(firstToken, className, decls, pubFuncs, priFuncs, env);
        }
        /**
         * @function name(argsList){statements}
         * 
         * 
         */
        public Node ParseFunction()
        {
            Token firstToken = null;
            string funcName = null;
            List<Identifier> parameters = null;
            Scope scope = null;
            Node body = null;

            ParseFunctionDeclaration(ref firstToken, ref funcName, ref scope, ref parameters);
            bool R = ReadToken("{");
            Debug.Assert(R);
            body = ParseBlock();
            R = ReadToken("}");
            Debug.Assert(R);
            R = body is Block;
            Debug.Assert(R);
            {
                Block block = (Block)body;
                try
                {
                    List<Node> returnArray =
                        block.statements.Where(s => s.token.Value == "return").ToList();
                    if (returnArray != null && returnArray.Count != 0
                        && !scope.table.Keys.Contains("->"))
                    {
                        scope.Put("->", "type", (ReturnStatement)returnArray[0]);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Warning: " + e.Message);
                    Console.WriteLine("Waning: function '" + funcName + "' has no return statement. " +
                        "[ 函数' " + funcName + "' 无返回值 ].");
                }
            }
            return new FunctionStatement(firstToken, funcName, parameters, scope, body);
        }
        public Node ParseFuncCallStatement()
        {
            FunctionCall funcCallExp = (FunctionCall)ParseFuncCallExpression();
            ReadToken(";").orThrow(token, "expect [;]");
            return funcCallExp;
        }
    }
}
