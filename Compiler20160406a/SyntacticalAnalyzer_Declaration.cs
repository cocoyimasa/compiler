﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Compiler.Ast;
using System.Diagnostics;

namespace Compiler
{
    public partial class SyntacticalAnalyzer
    {
        /*
        declaration → import-declaration¬
        declaration → constant-declaration¬
        declaration → variable-declaration¬
        declaration → typealias-declaration¬
        declaration → function-declaration¬
        **declaration → struct-declaration¬ 
        declaration → class-declaration¬
        declaration → initializer-declaration¬
        **declaration → deinitializer-declaration¬
        **declaration → extension-declaration¬
        **declaration → subscript-declaration¬
        **declaration → operator-declaration¬
        **declaration → precedence-group-declaration¬
        declarations → declaration ¬declarations ¬opt
         * **/
        public Value GetTypeSignature(Token token)
        {
            if (token.Type == TokenType.Integer)
            {
                return Value.INT;
            }
            else if (token.Type == TokenType.Float)
            {
                return Value.FLOAT;
            }
            else if (token.Type == TokenType.String)
            {
                return Value.STRING;
            }
            else if (token.Type == TokenType.Boolean)
            {
                return Value.BOOL;
            }
            else if (token.Type == TokenType.Identifier)
            {
                return new UserDefinedType(token.Value);
            }
            return null;
        }
        //import [*|{class[,classes]}] from package
        public Node ParseImportDeclaration()
        {
            List<string> modules = new List<string>();
            ImportDeclaration.ImportType type = 0;
            if(ReadToken("*"))
            {
                type = ImportDeclaration.ImportType.All_Modules;
                modules.Add("*");
                
            }
            else if (ReadToken("{"))
            {
                while (!ReadToken("}"))
                {
                    modules.Add(tokens[nextToken].Value);
                    bool R1 = ReadToken(TokenType.Identifier);
                    Debug.Assert(R1,"Expect [Identifier]");
                    if (!ReadToken(","))
                    {
                        break;
                    }
                }
                bool R2 = ReadToken("}");
                Debug.Assert(R2,"Expect [}] ");
                type = modules.Count > 1 ? ImportDeclaration.ImportType.Multi_Modules
                    : ImportDeclaration.ImportType.One_Module;
            }
            bool R = ReadToken(TokenType.FROM);
            R.orThrow("expect [from]");
            var package = tokens[nextToken];
            R = ReadToken(TokenType.Identifier);
            R.orThrow("expect [identifier]");
            R = ReadToken(";");
            R.orThrow("expect [;]");
            var decl = new ImportDeclaration(modules, package.Value);
            decl.type = type;
            return decl;
        }

        //@deprecated 已废弃
        public Node ParseVar(string className)
        {
            if (ReadToken(TokenType.Identifier))
            {
                Token tokenName = token;
                if (LookToken(":") &&
                   IsTypeBasicOrIdentify())
                {
                    bool R = ReadToken(":");
                    R.orThrow("expect [:]");
                    ReadTypeBasicOrIdentify();
                    tokenName = new Token(tokenName.Type, className + "::" + tokenName.Value, tokenName.Line, tokenName.Position);
                    Declaration decl = new Declaration(tokenName, new Identifier(token, token.Value));
                    if (!ReadToken(";"))
                    {
                        throw new CodeException(token, "缺少;");
                    }
                    return decl;
                }
                else
                {
                    if (ReadToken(";"))
                    {
                        tokenName = new Token(
                            tokenName.Type,
                            className + "::" + tokenName.Value,
                            tokenName.Line,
                            tokenName.Position);
                        Declaration decl = new Declaration(
                            tokenName,
                            new Identifier(Token.NewToken("Any"), "Any"));
                        return decl;
                    }
                    else
                    {
                        throw new CodeException(token, "缺少;");
                    }
                }
            }
            return null;
        }
        //!!!use constant and variable to refactor
        public Node ParseConstant()
        {
            VariableStatement decl = (VariableStatement)ParseConstantDeclaration();
            Node initializer = ParseInitializerDeclaration();
            decl.value = initializer;
            return decl;
        }
        public Node ParseVariable()
        {
            VariableStatement decl = (VariableStatement)ParseVariableDeclaration();
            Node initializer = ParseInitializerDeclaration();
            decl.value = initializer;
            return decl;
        }
        public Node ParseConstantDeclaration()
        {
            Token token = tokens[nextToken];
            if (ReadToken(TokenType.Identifier))
            {
                AccessLevel level = AccessLevel.DEFAULT;
                level = token.Value[0] == '@' ? AccessLevel.PRIVATE : level;
                if (level == AccessLevel.PRIVATE)
                {
                    token.Value = token.Value.Substring(1);
                }
                Identifier iden = new Identifier(token, token.Value);
                Value type = Value.ANY;
                if (ReadToken(":"))
                {
                    type = GetTypeSignature(tokens[nextToken]);
                    bool R = ReadTypeBasicOrIdentify();
                    R.orThrow("Expect Type Signature!");
                }
                VariableStatement st = new VariableStatement(Token.NewToken("let"), iden, type);
                st.access = level;
                st.modifier = Modifier.CONST;
                return st;
            }
            else if (ReadToken("("))// pattern
            {
                //access level should check each pattern internal identifier
                TupleNode pattern = (TupleNode)ParseTuple();
                Value type = Value.ANY;
                if (ReadToken(":"))
                {
                    type = GetTypeSignature(tokens[nextToken]);
                    bool R = ReadTypeBasicOrIdentify();
                    R.orThrow("Expect Type Signature!");
                }
                VariableStatement st = new VariableStatement(Token.NewToken("let"), pattern, type);
                st.modifier = Modifier.CONST;
                return st;
            }
            else
            {
                ;
            }
            return null;
        }
        public Node ParseVariableDeclaration()
        {
            Token token = tokens[nextToken];
            if(ReadToken(TokenType.Identifier))
            {
                AccessLevel level = AccessLevel.DEFAULT;
                level = token.Value[0] == '@' ? AccessLevel.PRIVATE : level;
                if(level == AccessLevel.PRIVATE)
                {
                    token.Value = token.Value.Substring(1);
                }
                Identifier iden = new Identifier(token, token.Value);
                Value type = Value.ANY;
                if (ReadToken(":"))
                {
                    type = GetTypeSignature(tokens[nextToken]);
                    bool R = ReadTypeBasicOrIdentify();
                    R.orThrow("Expect Type Signature!");
                }
                VariableStatement st = new VariableStatement(Token.NewToken("var"), iden, type);
                st.access = level;
                st.modifier = Modifier.VAR;
                return st;
            }
            else if(ReadToken("("))// pattern
            {
                //access level should check each pattern internal identifier
                TupleNode pattern = (TupleNode)ParseTuple();
                Value type = Value.ANY;
                if (ReadToken(":"))
                {
                    type = GetTypeSignature(tokens[nextToken]);
                    bool R = ReadTypeBasicOrIdentify();
                    R.orThrow("Expect Type Signature!");
                }
                VariableStatement st = new VariableStatement(Token.NewToken("var"), pattern, type);
                st.modifier = Modifier.VAR;
                return st;
            }
            else
            {
                ;
            }
            return null;
        }
        public Node ParseInitializerDeclaration()
        {
            if (ReadToken("="))
            {
                Token token = tokens[nextToken];
                int leftIndex = 0;
                Node val = null;
                if (FindToken(ref leftIndex, ";"))
                {
                    val = ParseExpression(leftIndex);
                }
                bool R = ReadToken(";");
                R.orThrow("expect [;]");
                return val != null ? val : new Node(Token.NewToken("0"));
            }
            else if (ReadToken(";"))
            {
                Debug.Write("Get the Semi-colon");
            }
            return new Node(Token.NewToken("0"));
        }
        //typedef A B
        public Node ParseTypealiasDeclaration()
        {
            Token newToken = tokens[nextToken];
            if (ReadToken(TokenType.Identifier))
            {
                Token oldToken = tokens[nextToken];
                bool R = ReadToken(TokenType.Identifier);
                R.orThrow("expect [identifier]");
                R = ReadToken(";");
                R.orThrow("expect [;]");
                return new TypeAliasDeclaration(newToken, oldToken);
            }
            Debug.Write("typedef error,no complete");
            return null;
        }
        public void ParseFunctionDeclaration(
            ref Token firstToken,
            ref string funcName,
            ref Scope scope,
            ref List<Identifier> parameters)
        {
            CheckFirstToken(ref firstToken, ref funcName, "function name not defined [ 函数名未定义 ].");

            if (ReadToken("("))
            {
                scope = ParseParameter(firstToken, out parameters);
                if (ReadToken(")"))
                {
                    if (scope != null &&
                        LookToken(":") &&
                        IsTypeBasicOrIdentify(1))
                    {
                        bool R = ReadToken(":");
                        R.orThrow("expect [:]");
                        R = ReadTypeBasicOrIdentify();
                        R.orThrow("expect [TypeBasicOrIdentify]");
                        scope.Put("->", "type", new Identifier(token, token.Value));
                    }
                    else
                    {
                        Console.WriteLine("Warning: function has no type signature.");
                    }
                }
                else
                {
                    throw new CodeException(token, "expect [)]'");
                }

            }
            else
            {
                throw new CodeException(token, "expect [(]");
            }
        }
        // [modifier] class Name [:parent[,parents]]  
        public Node ParseClassDeclaration()
        {
            // modifier not implement
            Token className = tokens[nextToken];
            bool R = ReadToken(TokenType.Identifier);
            R.orThrow("expect [identifier]");
            List<string> parentClass = new List<string>();
            if(ReadToken(":"))
            {
                while (!ReadToken("{") && !ReadToken(";")) // read parents
                {
                    parentClass.Add(tokens[nextToken].Value);
                    R = ReadToken(TokenType.Identifier);
                    R.orThrow("expect [identifier]");
                    if (!ReadToken(","))
                    {
                        break;
                    }
                }
            }
            return new ClassDeclaration(className, parentClass);
        }
        //class and interface share the same declaration
        public Node ParseInterfaceDeclaration()
        {
            return ParseClassDeclaration();
        }
    }
}
