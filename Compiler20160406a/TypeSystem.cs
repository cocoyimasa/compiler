﻿using Compiler.Ast;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler
{
    public class TypeSystem
    {
        public Dictionary<Identifier,AbstractType> emptyEnv=null;
        public static AbstractType intType = new TypeOperator("Integer", new List<AbstractType>());
        public static AbstractType boolType = new TypeOperator("Boolean", new List<AbstractType>());
        public static AbstractType floatType = new TypeOperator("Float", new List<AbstractType>());
        public static AbstractType stringType = new TypeOperator("String", new List<AbstractType>());
        public static AbstractType voidType = new TypeOperator("Void", new List<AbstractType>());

        protected static char nextVariableName = 'α';
        protected static int nextVariableId = 0;

        protected static string nextUniqueName()
        {
            var result = nextVariableName;
            nextVariableName = (char)(nextVariableName + 1);
            return result.ToString();
        }
        protected static Variable newVariable()
        {
            var result = nextVariableId;
            nextVariableId += 1;
            return new Variable(result);
        }
        public TypeSystem()
        {

        }
        public class AbstractType
        {
            public AbstractType()
            {

            }
            public override string ToString()
            {
                return base.ToString();
            }
        }

        public class Variable : AbstractType
        {
            public int id;
            public AbstractType instance = null;
            public string name;
            public Variable(int _id)
            {
                id = _id;
                name = nextUniqueName();
            }
            
            public override string ToString()
            {
                if (this.instance != null)
                {
                    return this.instance.ToString();
                }
                else
                {
                    return this.name;
                }
            }
        }
        public class TypeArray:AbstractType
        {
            public List<AbstractType> types;
            public TypeArray()
            {

            }
            public TypeArray(List<AbstractType> _types)
            {
                this.types = _types;
            }
            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("[");
                for (int i = 0; i < types.Count; i++)
                {
                    if (i == types.Count - 1)
                    {
                        sb.Append(types[i].ToString());
                    }
                    else
                    {
                        sb.Append(types[i].ToString() + ",");
                    }
                }
                return sb.Append("]").ToString();
            }
        }
        public class TypeOperator : AbstractType
        {
            public List<AbstractType> args;
            public string name;
            public TypeOperator()
            {

            }
            public TypeOperator(string name, List<AbstractType> args)
            {
                this.name = name;
                this.args = args;
            }
            public override string ToString()
            {
                if (args.Count == 0)
                {
                    return name;
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("[");
                    for(int i=0;i<args.Count;i++)
                    {
                        if(i==args.Count-1)
                        {
                            sb.Append(args[i].ToString());
                        }
                        else
                        {
                            sb.Append(args[i].ToString() + " -> ");
                        }
                    }
                    return sb.Append("]").ToString();
                }
            }
        }
        public AbstractType Function(AbstractType from,AbstractType to)
        {
            return new TypeOperator("->", new List<AbstractType>() { from,to});
        }
        public AbstractType Function(List<AbstractType> from, AbstractType to)
        {
            from.Add(to);
            return new TypeOperator("->", from);
        }
        public AbstractType Analyze(Node expr,Dictionary<Identifier,AbstractType> env)
        {
            return Analyze(expr,env,new HashSet<Variable>());
        }
        public AbstractType Analyze(Node expr,Dictionary<Identifier,AbstractType> env,HashSet<Variable> nongeneric)
        {
            if(expr is Identifier)
            {
                Identifier id = (Identifier)expr;
                return GetIdentifierType(id,env,nongeneric);
            }
            else if(expr is IntNum)
            {
                return intType;
            }
            else if(expr is FloatNum )
            {
                return floatType;
            }
            else if(expr is BoolNode)
            {
                return boolType;
            }
            else if(expr is StringNode)
            {
                return stringType;
            }
            else if(expr is IfStatement)
            {
                IfStatement stmt=(IfStatement)expr;
                var condType = Analyze(stmt.test, env, nongeneric);
                var thenType = Analyze(stmt.thenBody, env, nongeneric);
                var elseType = Analyze(stmt.elseBody, env, nongeneric);
                Unify(condType, boolType);
                Unify(thenType, elseType);
                return thenType;
            }
            else if(expr is FunctionCall)
            {
                FunctionCall stmt = (FunctionCall)expr;
                var funcType = Analyze(stmt.func, env,nongeneric);
                var argTypes = new List<AbstractType>();
                List<Node> nodes = (stmt.arguments as Argument).elements;
                foreach(var node in nodes)
                {
                    var type = Analyze(node, env, nongeneric);
                    argTypes.Add(type);
                }
                var resultType = newVariable();
                Unify(Function(argTypes, resultType), funcType);
                return resultType;
            }
            else if(expr is FunctionStatement)
            {
                FunctionStatement func = (FunctionStatement)expr;
                var newEnv = new Dictionary<Identifier, AbstractType>(env);
                var newNongeneric = new HashSet<Variable>(nongeneric);
                var argList=new List<AbstractType>();
                foreach(var item in func.parameters)
                {
                    var argType = newVariable();
                    newEnv[item] = argType;
                    newNongeneric.Add(argType);
                    argList.Add(argType);
                }
                var resultType = Analyze(func.body, newEnv, newNongeneric);
                AbstractType funcType = Function(argList, resultType);
                env.Add(Identifier.NewIdentifier(func.funcName), funcType);
                return funcType;
            }
            else if(expr is LambdaExpression)
            {
                LambdaExpression func = (LambdaExpression)expr;
                var newEnv = new Dictionary<Identifier, AbstractType>(env);
                var newNongeneric = new HashSet<Variable>(nongeneric);
                var argList = new List<AbstractType>();
                foreach (var item in func.parameters)
                {
                    var argType = newVariable();
                    newEnv.Add(item,argType);
                    newNongeneric.Add(argType);
                    argList.Add(argType);
                }
                var resultType = Analyze(func.body, newEnv, newNongeneric);
                return Function(argList, resultType);
            }
            else if(expr is WhileStatement)
            {
                WhileStatement stmt = (WhileStatement)expr;
                var condType = Analyze(stmt.test, env, nongeneric);
                var bodyType = Analyze(stmt.body, env, nongeneric);
                Unify(condType, boolType);
                return bodyType;
            }
            else if(expr is VariableStatement)
            {
                VariableStatement stmt=(VariableStatement)expr;
                var newEnv = new Dictionary<Identifier,AbstractType>(env);

                if(stmt.pattern is ArrayNode)
                {
                    ArrayNode vars=(ArrayNode)stmt.pattern;
                    ArrayNode vals=(ArrayNode)stmt.value;
                    for(int i=0;i<vars.array.Count;i++)
                    {
                        var definitionType = Analyze(vals.array[i], env, nongeneric);
                        newEnv.Add((Identifier)vars.array[i], definitionType);
                    }
                }
                else
                {
                    var definitionType = Analyze(stmt.value, env, nongeneric);
                    newEnv.Add((Identifier)stmt.pattern,definitionType);
                }
                return Analyze(stmt.value,env, nongeneric);
            }
            else if(expr is Argument)
            {
                Argument node = (Argument)expr;
                List<AbstractType> types = new List<AbstractType>();
                for (int i = 0; i < node.elements.Count; i++)
                {
                    var result = Analyze(node.elements[i], env, nongeneric);
                    types.Add(result);
                }
                return new TypeArray(types);
            }
            else if(expr is ArrayNode)
            {
                ArrayNode node=(ArrayNode)expr;
                List<AbstractType> types = new List<AbstractType>();
                for(int i=0;i<node.array.Count;i++)
                {
                    var result = Analyze(node.array[i], env, nongeneric);
                    types.Add(result);
                }
                return new TypeArray(types);
            }
            else if(expr is Block)
            {
                Block stmt = (Block)expr;
                var newEnv = new Dictionary<Identifier,AbstractType>(env);
                AbstractType last = null;
                for(int i=0;i<stmt.statements.Count;i++)
                {
                    var result = Analyze(stmt.statements[i], newEnv, nongeneric);
                    last = result;
                }
                if (last != null)
                {
                    return last;
                }
                else
                    return voidType;
            }
            else if(expr is ClassStatement)
            {

            }
            else if(expr is Declaration)
            {
                //
            }
            else if(expr is Field)
            {

            }
            else if(expr is MethodCall)
            {

            }
            else if(expr is ReturnStatement)
            {
                ReturnStatement stmt = (ReturnStatement)expr;
                return Analyze(stmt.returnValue, env, nongeneric);
            }
            else if(expr is TupleNode)
            {
                TupleNode node = (TupleNode)expr;
                List<AbstractType> types = new List<AbstractType>();
                for (int i = 0; i < node.elements.Count; i++)
                {
                    var result = Analyze(node.elements[i], env, nongeneric);
                    types.Add(result);
                }
                return new TypeArray(types);
            }
            return null;
        }
        protected AbstractType GetIdentifierType(Identifier id,Dictionary<Identifier,AbstractType> env,HashSet<Variable> nongeneric)
        {
            for (int i = 0; i < env.Keys.Count;i++ )
            {
                Identifier item = env.Keys.ElementAt(i);
                if (item.value==id.value)
                {
                    return Fresh(env[item], nongeneric);
                }
            }
                
            throw new Exception("Undefined symbol: " + id);
        }
        protected AbstractType Fresh(AbstractType t,HashSet<Variable> nongeneric)
        {
            var mapping = new Dictionary<Variable, Variable>();
            return Freshrec(t, nongeneric, mapping);
        }
        protected AbstractType Freshrec(AbstractType tp,HashSet<Variable> nongeneric,Dictionary<Variable,Variable> mapping)
        {
            var type = Prune(tp);
            if(type is Variable)
            {
                Variable v =(Variable)type;
                if (IsGeneric(v, nongeneric))
                {
                    if (!mapping.Keys.Contains(v))
                    {
                        mapping[v] = newVariable();
                    }
                    return mapping[v];
                }
                else
                    return v;
            }
            else if(type is TypeOperator)
            {
                TypeOperator t = (TypeOperator)type;
                var types = new List<AbstractType>();
                foreach(var x in t.args)
                {
                    types.Add(Freshrec(x, nongeneric, mapping));
                }
                return new TypeOperator(t.name, types);
            }
            return null;
        }
        protected void Unify(AbstractType t1,AbstractType t2)
        {
            var type1 = Prune(t1);
            var type2 = Prune(t2);
            if(type1 is Variable)
            {
                Variable a = (Variable)type1;
                if(a!=type2)
                {
                    if(OccursInType(a,type2))
                    {
                        throw new Exception("Recursive unification");
                    }
                    a.instance = type2;
                }
            }
            else if(type1 is TypeOperator && type2 is Variable)
            {
                Unify(type1, type2);
            }
            else if(type1 is TypeOperator && type2 is TypeOperator)
            {
                TypeOperator a=(TypeOperator)type1;
                TypeOperator b=(TypeOperator)type2;

                if(a.name != b.name || 
                    a.args.Count != b.args.Count)
                {
                    throw new Exception("Type mismatch: "+a.ToString()+" != "+b.ToString());
                }
                for (int i = 0; i < a.args.Count;i++ )
                {
                    Unify(a.args[i], b.args[i]);
                }
            }
        }
        protected AbstractType Prune(AbstractType type)
        {
            if (type is Variable)
            {
                Variable v = (Variable)type;
                if (v.instance != null)
                {
                    v.instance = Prune(v.instance);
                    return v.instance;
                }
            }
            return type;
        }
        protected bool IsGeneric(Variable v,HashSet<Variable> nongeneric)
        {
            return !(OccursIn(v,nongeneric));
        }
        protected bool OccursIn(Variable t,IEnumerable<AbstractType> list)
        {
            foreach(var item in list)
            {
                if(OccursInType(t,item))
                {
                    return true;
                }
            }
            return false;
        }
        protected bool OccursInType(Variable v,AbstractType type2)
        {
            var t = Prune(type2);
            if(t == v )
            {
                return true;
            }
            else if(t is TypeOperator)
            {
                TypeOperator to=(TypeOperator)t;
                return OccursIn(v, to.args);
            }
            return false;
        }

    }
}
/*
 *      public AbstractType addFunc   =new TypeOperator("+",     new List<AbstractType>());
        public AbstractType subFunc   =new TypeOperator("-",     new List<AbstractType>());
        public AbstractType mulFunc   =new TypeOperator("*",     new List<AbstractType>());
        public AbstractType divFunc   =new TypeOperator("/",     new List<AbstractType>());
        public AbstractType ltFunc    =new TypeOperator("<",     new List<AbstractType>());
        public AbstractType leFunc    =new TypeOperator("<=",    new List<AbstractType>());
        public AbstractType gtFunc    =new TypeOperator(">",     new List<AbstractType>());
        public AbstractType geFunc    =new TypeOperator(">=",    new List<AbstractType>());
        public AbstractType eqFunc    =new TypeOperator("=",     new List<AbstractType>());
        public AbstractType andFunc   =new TypeOperator("and",   new List<AbstractType>());
        public AbstractType orFunc    =new TypeOperator("or",    new List<AbstractType>());
        public AbstractType notFunc   =new TypeOperator("not",   new List<AbstractType>());
        public AbstractType neFunc    =new TypeOperator("<>",    new List<AbstractType>());
        public AbstractType trueType  =new TypeOperator("true",  new List<AbstractType>());
        public AbstractType falseType =new TypeOperator("false", new List<AbstractType>());
 */