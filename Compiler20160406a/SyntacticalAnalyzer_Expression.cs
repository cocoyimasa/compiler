﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Compiler.Ast;
using System.Diagnostics;

namespace Compiler
{
    public partial class SyntacticalAnalyzer
    {
        /*
                  expression → 
        try-operator¬ ¬prefix-expression ¬binary-expressions¬¬
        expression-list → expression¬ | expression¬,¬expression-list¬
        基本表达式：
        primary-expression → identifier¬generic-argument-clause¬opt¬
        primary-expression → literal-expression¬
        primary-expression → this-expression¬
        primary-expression → superclass-expression¬
        primary-expression → closure-expression¬
        primary-expression → parenthesized-expression¬
        primary-expression → tuple-expression¬
        primary-expression → implicit-member-expression¬
        字典或数组表达式：
        literal-expression → literal¬
        literal-expression → array-literal¬| dictionary-literal¬
        array-literal → [¬array-literal-items¬opt¬]¬
        array-literal-items → 
        array-literal-item¬,¬array-literal-items¬
        array-literal-item → expression¬
        dictionary-literal → {dictionary-literal-items¬}¬| {}¬
        dictionary-literal-items → 
        dictionary-literal-item¬,¬dictionary-literal-items¬
        dictionary-literal-item → expression¬:¬expression¬
        this表达式：
        this-expression → this|
        this-method-expression¬|
        this-subscript-expression¬
        this-method-expression → this¬.¬identifier¬
        this-subscript-expression → this¬[¬expression-list¬]¬
        lambda表达式：
        closure-expression → [¬closure-signature]{¬¬statements¬}¬
        closure-signature → 
        capture-list¬ ¬closure-parameter-clause function-result¬
        closure-signature → capture-list
        closure-parameter-clause → (¬)¬|(¬closure-parameter-list¬)-  identifier-list¬
        closure-parameter-list → closure-parameter-  closure-parameter¬,¬closure-parameter-list¬
        closure-parameter → closure-parameter-name¬type-annotation-opt¬
        closure-parameter → closure-parameter-name¬type-annotation-...¬
        closure-parameter-name → identifier¬
        capture-list → [¬capture-list-items¬]¬
        capture-list-items → capture-list-item-  capture-list-item¬,¬capture-list-items¬
        capture-list-item → ¬expression¬
         * **/
        /*
         * 1.Value: 22,22.2,"aaa",true 值类型
         * 2.Id: a,b,c 标识符 （对象名，函数名，变量名）
         * 3.ArithExp:2+2,a and b,a*3 算术表达式
         * 4.Lambda:lambda(x y)(let a=(a+b)) 匿名函数
         * 5.Expression with ():(a+b) 括号表达式
         * 6.ArrayList:[1,3,4,5] 列表
         * 7.funcCall  函数调用
         * 8.new Class() 匿名对象
         * 9.MethodCall:obj.method  方法调用
         * 10.Property:obj.property 属性
         */
        public Node ParseExpression(int endIndex)
        {
            List<Node> exps = new List<Node>();
            if (ReadToken(TokenType.LAMBDA))//4.Lambda:lambda(x y)(let a=(a+b)) 匿名函数
            {
                return ParseLambda();
            }
            else if (LookToken("["))//6.ArrayList:[1,3,4,5] 列表
            {
                return ParseArray();
            }
            else if (LookToken(TokenType.Identifier) && LookToken("(", 1))//7.funcCall  函数调用
            {
                bool R = ReadToken(TokenType.Identifier);
                R.orThrow("expect [identifier]");
                return ParseFuncCallExpression();
            }
            else if (LookToken(TokenType.Identifier) && LookToken(".", 1))//
            {
                bool R = ReadToken(TokenType.Identifier);
                R.orThrow("expect [identifier]");
                R = ReadToken(".");
                R.orThrow("expect [.]");
                if (ReadToken(TokenType.Identifier))
                {
                    if (LookToken("("))//9.MethodCall:obj.method  方法调用
                    {
                        return ParseMethodCall();
                    }
                    else //10.Property:obj.property 属性
                    {
                        return ParseField();
                    }
                }
                else
                {
                    throw new CodeException(token, ".点语法格式错误.");
                }
            }
            else if (ReadToken(TokenType.NEW))//8.new Class() 匿名对象
            {
                return ParseMethodCall();
            }
            else //1,2,3,5 四种基本表达式
            {
                return ParsePrim(endIndex);
            }
        }
        public Node ParseLiteralExpression()
        {
            return null;
        }

        public Node ParseDictionaryExpression()
        {
            return null;
        }
        public Node ParseThisExpression()
        {
            return null;
        }
        //lambda(x:int,y:int):int{}
        public Node ParseLambda()
        {
            List<Identifier> parameters = null;
            Node body = null;
            Scope scope = ParseParameter(token, out parameters);
            Token firstToken = token;
            if (ReadToken("("))
            {
                scope = ParseParameter(firstToken, out parameters);
                if (ReadToken(")"))
                {
                    if (scope != null &&
                        LookToken(":") &&
                        IsTypeBasicOrIdentify(1))
                    {
                        bool R = ReadToken(":");
                        R.orThrow("expect [:]");
                        ReadTypeBasicOrIdentify();
                        scope.Put("->", "type", new Identifier(token, token.Value));
                    }
                    else
                    {
                        Console.WriteLine("Warning: function has no type signature.");
                    }
                }
                else
                {
                    throw new CodeException(token, "expect a ')'[ 缺少')' ].");
                }

            }
            else
            {
                throw new CodeException(token, "expect a '('[ 缺少'(' ].");
            }
            if (ReadToken("{"))
            {
                body = ParseBlock();
                if (!ReadToken("}"))
                {
                    throw new CodeException(token, "expect a '}'[ 缺少'}' ].");
                }
                else
                {
                }
            }
            else
            {
                throw new CodeException(token, "expect a '{'[ 缺少'{' ].");
            }
            if (body is Block)
            {
                Block block = (Block)body;
                try
                {
                    List<Node> returnArray =
                        block.statements.Where(s => s.token.Value == "return").ToList();
                    if (returnArray != null && returnArray.Count != 0
                        && !scope.table.Keys.Contains("->"))
                    {
                        scope.Put("->", "type", (ReturnStatement)returnArray[0]);
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Warning: " + e.Message);
                    Debug.WriteLine("Waning: function '" + firstToken.Value + "' has no return statement. " +
                        "[ 函数' " + firstToken.Value + "' 无返回值 ].");
                }
            }
            return new LambdaExpression(firstToken, parameters, scope, body);
        }
        //public enum ArrayState { 
        //    ArrayBegin,
        //    ArrayEnd,
        //    ArrayElement,
        //    ArrayUnknow
        //}
        public Node ParseArray()
        {
            ArrayNode node = new ArrayNode(Token.NewToken("array"), null);
            ArrayNode current = node;
            while (!LookToken("=") && !LookToken("{") && !LookToken(";"))
            {
                if (ReadToken("["))
                {
                    ArrayNode elem = new ArrayNode(Token.NewToken("["), current);
                    current.array.Add(elem);
                    current = elem;
                }
                else if (ReadToken("]"))
                {
                    ArrayNode elem = new ArrayNode(Token.NewToken(")"), current);
                    current.array.Add(elem);
                    current = current.parent;
                    if (current == null)
                    {
                        break;
                    }
                }
                else
                {
                    if (!LookToken(","))
                    {
                        ReadArrayValue();
                        Node exp = ProcessNode(token);
                        current.array.Add(exp);
                    }
                    else
                    {
                        bool R = ReadToken(",");
                        R.orThrow("expect [,]");
                        //continue;
                    }
                }
            }
            return node;
        }
        
        
        public bool ProcessNegative(ref List<Node> exps, TokenType type)
        {
            if (token.Value == "-" &&
                   ReadToken(type))//开头的负整数处理
            {
                //exps.Add(Identifier.NewIdentifier("("));
                token.Value = "-" + token.Value;
                if (type == TokenType.Integer)
                {
                    exps.Add(new IntNum(token));
                }
                else
                {
                    exps.Add(new FloatNum(token));
                }
                //exps.Add(Identifier.NewIdentifier(")"));
                return true;
            }
            return false;
        }
        public Node ParsePrim(int endIndex)
        {
            List<Node> exps = new List<Node>();
            int count = 0;
            while (
                count < endIndex &&
                !LookToken("]") &&
                !LookToken("{") &&
                !LookToken("}") &&
                !LookToken(";") &&
                !LookToken(","))
            {
                ReadAny();
                count++;
                bool flag = false;
                //前面为符号，折叠负号表达式
                if (LookToken(TokenType.Operator,-1) && LookToken("-"))
                {
                    flag = ProcessNegative(ref exps, TokenType.Integer);
                    if (!flag)
                    {
                        flag = ProcessNegative(ref exps, TokenType.Float);
                    }
                }
                else { 
                    exps.Add(ProcessNode(token));
                }
            }
            if (exps.Count == 0)
            {
                throw new CodeException(token, "表达式不能为空");
            }
            if (exps.Count == 1)
            {
                return exps[0];
            }
            else if (exps.Count == 2)// not a | -b
            {
                Token tok = Token.NewToken("primitive");
                FunctionCall call = new FunctionCall(tok,
                    Identifier.NewIdentifier(exps[0].token.Value),
                    new Argument(tok, exps.Skip(1).ToList()));
                return call;
            }
            else
            {
                return ParseArithList(exps);
            }
        }
        public Node ParseArithList(List<Node> exps)
        {
            Dictionary<string, int> table = new Dictionary<string, int>()
            {
                {"(",10},{")",0},{"or",1},{"and",2},{"not",3},
                {">",4},{"<",4},{">=",4},{"<=",4},{"=",4},
                {"<>",4},
                {"+",5},{"-",5},{"*",6},{"/",6}
            };
            List<Node> opList = new List<Node>();
            Stack<Node> opStack = new Stack<Node>();
            //(-100)+d*2+100*0.3 -》 -100 d 2 * 100 0.3 * + +
            for (int i = 0; i < exps.Count; i++)
            {
                Token item = exps[i].token;
                if (item.Type == TokenType.Operator)
                {
                    if (item.Value == ")")
                    {
                        Node top = opStack.Pop();
                        while (top.token.Value != "(" && opStack.Count>0)
                        {
                            opList.Add(top);
                            top = opStack.Pop();
                        }
                    }
                    else if (opStack.Count != 0 &&
                        table[item.Value] <= table[opStack.Peek().token.Value])
                    {
                        Node top = opStack.Pop();
                        opList.Add(top);
                        opStack.Push(exps[i]);
                    }
                    else
                    {
                        opStack.Push(exps[i]);
                    }
                }
                else
                {
                    opList.Add(exps[i]);
                }
            }
            while (opStack.Count != 0)
            {
                opList.Add(opStack.Pop());
            }
            opStack.Clear();
            for (int i = 0; i < opList.Count; i++)
            {
                Token item = opList[i].token;
                if (item.Type == TokenType.Operator)
                {
                    List<Node> args = new List<Node>();
                    if (item.Value == "not")
                    {
                        args.Add(opStack.Pop());
                    }
                    else
                    {
                        Node args2 = opStack.Pop();
                        Node args1 = opStack.Pop();
                        args.Add(args1);
                        args.Add(args2);
                    }
                    FunctionCall call = new FunctionCall(
                        Token.NewToken("primitive"),
                        Identifier.NewIdentifier(item.Value),
                        new Argument(Token.NewToken("primitive"), args));
                    opStack.Push(call);
                }
                else
                {
                    opStack.Push(opList[i]);
                }
            }
            return opStack.Pop();
        }
        public Node ProcessNode(Token token)
        {
            if (token.Type == TokenType.Integer)
            {
                return new IntNum(token);
            }
            else if (token.Type == TokenType.Float)
            {
                return new FloatNum(token);
            }
            else if (token.Type == TokenType.String)
            {
                return new StringNode(token);
            }
            else if (token.Type == TokenType.Boolean)
            {
                return new BoolNode(token);
            }
            else if (token.Type == TokenType.Identifier)
            {
                return new Identifier(token, token.Value);
            }
            else if (token.Type == TokenType.Operator)
            {
                if (token.Value == "{" || token.Value == "}"
                    || token.Value == "(" || token.Value == ")" ||
                    token.Value == "[" || token.Value == "]" ||
                    token.Value == ";" || token.Value == ",")
                {
                    return new Delimeter(token);
                }
                else
                {
                    return new Identifier(token, token.Value);
                }
            }
            return null;
        }
        // parameter 形参
        public Scope ParseParameter(Token funcName, out List<Identifier> parameters)
        {
            parameters = new List<Identifier>();
            Scope scope = new Scope();
            while (ReadToken(TokenType.Identifier))
            {
                bool R = false;
                Dictionary<string, object> dict = new Dictionary<string, object>();
                Identifier id = new Identifier(token, token.Value);
                parameters.Add(id);
                if (LookToken(":") &&
                    IsTypeBasicOrIdentify(1))
                {
                    R = ReadToken(":");
                    R.orThrow("expect [:]");
                    R = ReadTypeBasicOrIdentify();
                    R.orThrow("expect [TypeBasicOrIdentify]");
                    Identifier type = new Identifier(token, token.Value);
                    dict.Add("type", type);
                    if (!ReadToken(","))
                    {
                        break;
                    }
                }
                else if (LookToken(","))
                {
                    R = ReadToken(",");
                    R.orThrow("expect [,]");
                    Identifier type = new Identifier(Token.NewToken("Any"), "Any");
                    dict.Add("type", type);
                }
                else if (LookToken(")"))
                {
                    break;
                }
                else
                {
                    throw new CodeException(funcName, "parameters declaration is not complete[ 函数参数类型声明不完整 ].");
                }
                scope.PutProperties(id.value, dict);
            }
            return scope;
        }
        // @@@ to be refactor!!!
        public Node ParseTuple()
        {
            TupleNode node = new TupleNode(Token.NewToken("tuple"), null);
            TupleNode current = node;
            while (!LookToken(":") && !LookToken("{") && !LookToken(";"))
            {
                if (ReadToken("("))
                {
                    TupleNode elem = new TupleNode(Token.NewToken("("), current);
                    current.elements.Add(elem);
                    current = elem;
                }
                else if (ReadToken(")"))
                {
                    TupleNode elem = new TupleNode(Token.NewToken(")"), current);
                    current.elements.Add(elem);
                    current = current.parent;
                    if (current == null)
                    {
                        break;
                    }
                }
                else
                {
                    if (!LookToken(","))
                    {
                        ReadArrayValue();
                        Node exp = ProcessNode(token);
                        current.elements.Add(exp);
                    }
                    else
                    {
                        ReadToken(",").orThrow("expect [,]");
                        //continue;
                    }
                }
            }
            return node;
        }
        //argument 实参
        public Node ParseArgument()
        {
            TupleNode node = (TupleNode)ParseTuple();
            return new Argument(Token.NewToken("arguments"), node.elements);
        }
        public Node ParseFuncCallExpression()
        {
            Identifier func = new Identifier(token, token.Value);
            Argument arguments = new Argument();
            if (ReadToken("("))
            {
                arguments = (Argument)ParseArgument();
            }
            else
            {
                throw new CodeException(token, "缺少(");
            }
            return new FunctionCall(func.token, func, arguments);
        }
    }
}
