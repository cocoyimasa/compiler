﻿using Compiler.Ast;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler
{
    public partial class SyntacticalAnalyzer /*: SyntaxBase*/
    {
        private int nextToken = 0;
        public List<Token> tokens;
        public Token token = new Token();

        public SyntacticalAnalyzer()
        {
        }
        public bool isFinish()
        {
            if (tokens.Count == nextToken)
            {
                return true;
            }
            return false;
        }
        public void Forword()
        {
            nextToken++;
        }
        public void Forword(int step)
        {
            nextToken += step;
        }
        public bool ReadToken(TokenType type)
        {
            if (isFinish())
            {
                return false;
            }
            else if (tokens[nextToken].Type == type)
            {
                token = tokens[nextToken];
                Forword();
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool ReadToken(string name)
        {
            if (isFinish())
            {
                return false;
            }
            else if (tokens[nextToken].Value == name)
            {
                token = tokens[nextToken];
                Forword();
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool LookToken(TokenType type)
        {
            if (isFinish())
            {
                return false;
            }
            else if (tokens[nextToken].Type == type)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool LookToken(string name)
        {
            if (isFinish())
            {
                return false;
            }
            else if (tokens[nextToken].Value == name)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool LookToken(TokenType type, int step)
        {
            if (isFinish())
            {
                return false;
            }
            else if (tokens.Count <= nextToken + step)
            {
                return false;
            }
            else if (tokens[nextToken + step].Type == type)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool LookToken(string name, int step)
        {
            if (isFinish())
            {
                return false;
            }
            else if (tokens.Count <= nextToken + step)
            {
                return false;
            }
            else if (tokens[nextToken + step].Value == name)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool FindToken(ref int index, TokenType type)
        {
            while (true)
            {
                if (tokens.Count <= nextToken + index)
                {
                    return false;
                }
                else if (tokens[nextToken + index].Type != type)
                {
                    index++;
                }
                else
                {
                    break;
                }
            }
            return true;
        }
        public bool FindToken(ref int index, string value)
        {
            while (true)
            {
                if (tokens.Count <= nextToken + index)
                {
                    return false;
                }
                else if (tokens[nextToken + index].Value != value)
                {
                    index++;
                }
                else
                {
                    break;
                }
            }
            return true;
        }
        // basic type or custom defined type signature 
        public bool IsTypeBasicOrIdentify()
        {
            return LookToken(TokenType.INT) ||
                LookToken(TokenType.FLOAT) ||
                LookToken(TokenType.STRING) ||
                LookToken(TokenType.BOOL) ||
                LookToken(TokenType.Identifier)
                ;
        }
        public bool IsTypeBasicOrIdentify(int step)
        {
            return LookToken(TokenType.INT, step) ||
                LookToken(TokenType.FLOAT, step) ||
                LookToken(TokenType.STRING, step) ||
                LookToken(TokenType.BOOL, step) ||
                LookToken(TokenType.Identifier, step)
                ;
        }
        public bool ReadTypeBasicOrIdentify()
        {
            return ReadToken(TokenType.INT) ||
                ReadToken(TokenType.FLOAT) ||
                ReadToken(TokenType.STRING) ||
                ReadToken(TokenType.BOOL) ||
                ReadToken(TokenType.Identifier)
                ;
        }
        //basic value or identify 
        public bool IsValue(TokenType type)
        {
            switch (type)
            {
                case TokenType.Integer:
                case TokenType.Float:
                case TokenType.Boolean:
                case TokenType.String:
                case TokenType.Identifier:
                    return true;
                default:
                    return false;
            }
        }

        public bool ReadValue()
        {
            return
                ReadToken(TokenType.Identifier) ||
                ReadToken(TokenType.Integer) ||
                ReadToken(TokenType.Float) ||
                ReadToken(TokenType.String) ||
                ReadToken(TokenType.Boolean) ||
                ReadToken("(") ||
                ReadToken(")");
        }
        public bool ReadArrayValue()
        {
            return
                ReadToken(TokenType.Identifier) ||
                ReadToken(TokenType.Integer) ||
                ReadToken(TokenType.Float) ||
                ReadToken(TokenType.String) ||
                ReadToken(TokenType.Boolean);
        }
        public bool ReadAny()
        {
            return
                ReadToken(TokenType.Identifier) ||
                ReadToken(TokenType.Integer) ||
                ReadToken(TokenType.Float) ||
                ReadToken(TokenType.String) ||
                ReadToken(TokenType.Boolean) ||
                ReadToken(TokenType.Operator);
        }

        public Node ParseProgram()
        {
            List<Node> elements = new List<Node>();
            while (true)
            {
                if (ReadToken(TokenType.FUNCTION))
                {
                    elements.Add(ParseFunction());
                }
                else if (LookToken(TokenType.Identifier) && LookToken("(", 1))
                {
                    ReadToken(TokenType.Identifier).orThrow("expect [Identifier]");
                    elements.Add(ParseFuncCallStatement());
                }
                else if (ReadToken(TokenType.LET))
                {
                    elements.Add(ParseConstant());
                }
                else if (ReadToken(TokenType.VAR))
                {
                    elements.Add(ParseVariable());
                }
                else if (ReadToken(TokenType.CLASS))
                {
                    elements.Add(ParseClass());
                }
                else if (ReadToken(TokenType.INTERFACE))
                {
                    elements.Add(ParseInterface());
                }
                else if (ReadToken(TokenType.DEFINE))
                {

                }
                else if (ReadToken(TokenType.IMPORT))
                {
                    elements.Add(ParseImportDeclaration());
                }
                else if (ReadToken(TokenType.TYPEDEF))
                {
                    elements.Add(ParseTypealiasDeclaration());
                }
                else if (LookToken(TokenType.Identifier) && LookToken(".", 1))
                {
                    ReadToken(TokenType.Identifier).orThrow("expect [Identifier]");
                    elements.Add(ParseMethodCall());
                }
                else
                {
                    break;
                }
            }
            return new TupleNode(Token.NewToken("program"), elements, null);
        }
        public void CheckFirstToken(ref Token firstToken, ref string name, string error)
        {
            if (ReadToken(TokenType.Identifier))
            {
                name = token.Value;
                firstToken = token;
            }
            else
            {
                throw new CodeException(token, error);
            }
        }
        public void ParseMacro()
        {

        }
        
        public Node ParseBlock()
        {
            List<Node> stList = ParseStatementList();
            return new Block(Token.NewToken("block"), stList);
        }
        
        public Node ParseVirtualFunction()
        {
            Token firstToken = null;
            string funcName = null;
            List<Identifier> parameters = null;
            Scope scope = null;
            ParseFunctionDeclaration(ref firstToken, ref funcName, ref scope, ref parameters);
            return new VirtualFunctionDeclaration(firstToken, funcName, parameters, scope);
        }
        
        public Node ParseMethodCall()
        {
            Argument arguments = null;
            Identifier func = null;
            Identifier className = null;
            className = new Identifier(token, token.Value);
            if (LookToken(".") &&
                LookToken(TokenType.Identifier, 1))//A.Fun(...)
            {
                Debug.Assert(ReadToken("."));
                Debug.Assert(ReadToken(TokenType.Identifier));
                token.Value = className.value + "::" + token.Value;
                func = new Identifier(token, token.Value);
            }
            else if (LookToken("("))//new Ctor(...)
            {
                token.Value = className.value + "::" + token.Value;
                func = new Identifier(token, token.Value);
            }
            else
            {
                throw new CodeException(className.token, "缺少.");
            }
            arguments = new Argument();
            if (ReadToken("("))
            {
                arguments = (Argument)ParseArgument();
            }
            if (!ReadToken(";"))
            {
                throw new CodeException(className.token, "缺少;");
            }
            return new MethodCall(func.token, className, func, arguments);
        }
        public Node ParseField()
        {
            return null;//same as ParseVar
        }


    }
}
/*
 * ArrayState state = ArrayState.ArrayBegin;
            bool arrayDone = false;
            bool finished = false; 
            while(ReadAny()){
                List<Node> innerElems = new List<Node>();
                switch (state) {
                    case ArrayState.ArrayBegin:
                        {
                            if (token.Value == "[")
                            {
                                state = ArrayState.ArrayElement;
                            }
                            else if (token.Value == "]")
                            {
                                state = ArrayState.ArrayEnd;
                            }
                            else if (token.Value == ",")
                            {
                                state = ArrayState.ArrayBegin;
                            }
                            else if (token.Value == ";")
                            {
                                state = ArrayState.ArrayUnknow;//结束
                            }
                            else
                            {
                                state = ArrayState.ArrayElement;
                            }
                        }
                        break;
                    case ArrayState.ArrayElement:
                        {
                            //bug
                            Node node = ProcessNode(token);
                            if (!LookToken(",") && !LookToken("]"))
                            {
                                innerElems.Add(ParseExpression());
                            }
                            else
                            {
                                innerElems.Add(node);
                            }
                            state = ArrayState.ArrayBegin;
                        }
                        break;
                    case ArrayState.ArrayEnd:
                        arrayDone = true;
                        state = ArrayState.ArrayBegin;
                        break;
                    case ArrayState.ArrayUnknow:
                        finished = true;
                        break;
                }
                if (arrayDone)
                {
                    elements.Add(new ArrayNode(Token.NewToken("array"), innerElems));
                    arrayDone = false;
                }
                if (finished)
                {
                    break;
                }
            }
 */