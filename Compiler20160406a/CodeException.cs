﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler
{
    static class Leaf
    {
        public static void orThrow(this bool bVal,string msg)
        {
            if (!bVal) throw new Exception(msg);
        }
        public static void orThrow(this bool bVal, Token tok, string msg)
        {
            if (!bVal) throw new CodeException(tok, msg);
        }
    }
    [DebuggerDisplay("{detailMsg}")]
    class CodeException : Exception
    {
        protected string name;
        protected int position;
        protected int line;
        protected string detailMsg;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Position
        {
            get { return position; }
            set { position = value; }
        }

        public int Line
        {
            get { return line; }
            set { line = value; }
        }
        public string DetailMsg
        {
            get { return detailMsg; }
            set { detailMsg = value; }
        }
        public CodeException()
        { }
        public CodeException(string _name, int _line, int _pos, string errMsg)
            : base(_name + " at " + _line + ", " + _pos + errMsg)
        {
            name = _name;
            position = _pos;
            line = _line;
            detailMsg = name + " at " + line + ", " + position + errMsg;
        }
        public CodeException(Token token, string errMsg)
            : base(token.Value + " at " + token.Position + ", " + token.Line + errMsg)
        {
            name = token.Value;
            position = token.Position;
            line = token.Line;
            detailMsg = name + " at " + line + ", " + position + errMsg;
        }
    }
}
