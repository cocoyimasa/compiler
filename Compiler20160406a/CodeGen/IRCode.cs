﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.CodeGen
{
    public enum IRType
    {
        MOV, ADD, SUB, MUL, DIV,
        CMP,
        JMP, JMPF, JE, JMPT,
        STORE, LOAD,
        PUSH, POP,
        EQ, UNEQ,
        LT, GT,
        LE, GE,
        AND, OR,
        LABEL, FUNC, PARAM, RET, CALL,
        IConst, FConst,
        BYTE, WORD, DW, QW
    }
    public class IRCode
    {
        public IRType opType;
        public string op1;
        public string op2;
        private int count;

        public int Count
        {
            get { return count; }
            set { count = value; }
        }

        public IRCode()
        {
            count = 0;
            op1 = "";
            op2 = "";
        }
        public IRCode(IRType _opType, string _op1="", string _op2="")
        {
            this.opType = _opType;
            this.op1 = _op1;
            this.op2 = _op2;
            if (_op1 == "")
            {
                count = 0;
            }
            else if (_op2 == "")
            {
                count = 1;
            }
            else
                count = 2;
        }

        public void CheckCount()
        {
            if(op1=="")
            {
                Count = 0;
            }
            else if (op2 == "")
            {
                Count = 1;
            }
            else
            {
                Count = 2;
            }
        }

        public override string ToString()
        {
            CheckCount();
            switch(count)
            {
                case 0:
                    return opType.ToString().ToLower();
                case 1:
                    return opType.ToString().ToLower() + " " + op1;
                case 2:
                    return opType.ToString().ToLower() + " " + op1 + " " + op2;
            }
            return "";
        }
    }
}
