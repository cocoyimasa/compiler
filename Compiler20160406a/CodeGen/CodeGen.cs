﻿using Compiler.Ast;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.CodeGen
{
    public class CodeGenerator
    {
        /*
         * Argument
         * ArrayNode
         * Block
         * BoolNode
         * ClassStatement
         * Declaration
         * Delimeter
         * Field
         * FloatNum
         * FunctionCall
         * FunctionStatement
         * Identifier
         * IfStatement
         * IntNum
         * LambdaExpression
         * LetStatement
         * MethodCall
         * Node
         * Record
         * ReturnStatement
         * StringNode
         * TupleNode
         * WhileStatement
         */
        public List<IRCode> codes=new List<IRCode>();
        private int label = 0;
        private int id = 0;
        public void Generate(Argument              node)
        {}
        public void Generate(ArrayNode             node)
        {
            int len = node.array.Count;
            IRCode code = new IRCode(IRType.DW, "data" + id, len.ToString());
            codes.Add(code);
            for (int i = 0; i < len; i++)
            {
                node.array[i].Generate(this);                
            }
        }
        public void Generate(Block                 node)
        {
            for(int i=0;i<node.statements.Count;i++)
            {
                node.statements[i].Generate(this);
            }
        }
        public void Generate(BoolNode              node)
        {
            IRCode code = new IRCode(IRType.PUSH, node.value.ToString());
            codes.Add(code);
        }
        public void Generate(ClassStatement        node)
        {

        }
        public void Generate(Declaration           node)
        {}
        public void Generate(Delimeter             node)
        {}
        public void Generate(Field                 node)
        {}
        public void Generate(FloatNum              node)
        {
            IRCode code = new IRCode(IRType.PUSH, node.value.ToString());
            codes.Add(code);
        }
        public void Generate(FunctionCall          node)
        {
            IRCode code = null;
            List<Node> args=node.arguments.elements;
            for (int i = 0; i < args.Count;i++ )
            {
                args[i].Generate(this);
            }
            code = new IRCode(IRType.CALL, node.func.value);
            codes.Add(code);
            //primitives
        }
        public void Generate(FunctionStatement     node)
        {
            IRCode code = new IRCode(IRType.FUNC, node.funcName);
            codes.Add(code);
            for(int i=node.parameters.Count-1;i>=0;i--)
            {
                code = new IRCode(IRType.PARAM, node.parameters[i].value);
                codes.Add(code);
            }
            node.body.Generate(this);
        }
        public void Generate(Identifier            node)
        {
        }
        public void Generate(IfStatement           node)
        {
            node.test.Generate(this);
            string labelF="elseIf"+label++;
            IRCode code=new IRCode(IRType.JMPF,labelF);
            codes.Add(code);
            node.thenBody.Generate(this);
            string labelEnd = "ifend" + label++;
            code = new IRCode(IRType.JMP, labelEnd);
            codes.Add(code);
            code = new IRCode(IRType.LABEL, labelF);
            codes.Add(code);
            node.elseBody.Generate(this);
            code = new IRCode(IRType.LABEL, labelEnd);
            codes.Add(code);
        }
        public void Generate(IntNum                node)
        {
            IRCode code = new IRCode(IRType.PUSH, node.value.ToString());
            codes.Add(code);
        }
        public void Generate(LambdaExpression      node)
        {

        }
        public void Generate(VariableStatement          node)
        {
            if(node.pattern is ArrayNode)
            {

            }
            else if(node.pattern is Identifier)
            {
                string name=node.pattern.token.Value;
                node.value.Generate(this);
                IRCode code=new IRCode(IRType.STORE,name);
                codes.Add(code);
            }
        }
        public void Generate(MethodCall            node)
        {}
        public void Generate(Node                  node)
        {}
        public void Generate(Record                node)
        {}
        public void Generate(ReturnStatement       node)
        {
            node.returnValue.Generate(this);
            IRCode code = new IRCode(IRType.RET);
            codes.Add(code);
        }
        public void Generate(StringNode            node)
        {
            IRCode code = new IRCode(IRType.PUSH, node.token.Value);
            codes.Add(code);
        }
        public void Generate(TupleNode             node)
        {
            foreach(var item in node.elements)
            {
                item.Generate(this);
            }
        }
        public void Generate(WhileStatement        node)
        {
            node.test.Generate(this);
            string labelF="whileNext"+label;
            IRCode code = new IRCode(IRType.JMPF, labelF);
            codes.Add(code);
            node.body.Generate(this);
            code = new IRCode(IRType.LABEL, labelF);
            codes.Add(code);

        }
    }
}
