﻿using Compiler.Ast;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler
{
    public interface SyntaxBase
    {
        Node ParseProgram();
        Node ParseFunction();
        List<Node> ParseStatementList();
        Node ParseVar(string className);
        Node ParseClass();
        Node ParseBlock();
        Scope ParseParameter(Token funcName, out List<Identifier> parameters);
        Node ParseFuncCall();
        Node ParseMethodCall();
        Node ParseArgument();
        Node ParseLet();
        Node ParseField();
        Node ParseIf();
        Node ParseWhile();
        Node ParseLambda();
        Node ParseArray();
        Node ParseExpression(int endIndex);
        bool ProcessNegative(ref List<Node> exps, TokenType type);
        Node ParsePrim(int endIndex);
        Node ParseArithList(List<Node> exps);
        Node ProcessNode(Token token);
    }
}
