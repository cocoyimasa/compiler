﻿using Compiler.CodeGen;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.Ast
{
    [DebuggerDisplay("{funcName}{parameters}")]
    public class VirtualFunctionDeclaration : Node
    {
        public string funcName;
        public Scope paramTable;
        public List<Identifier> parameters = new List<Identifier>();
        public VirtualFunctionDeclaration()
        {

        }
        public VirtualFunctionDeclaration(Token tok, string funcName, List<Identifier> parameters, Scope paramTable)
            : base(tok)
        {
            this.funcName = funcName;
            this.paramTable = paramTable;
            this.parameters = parameters;
        }

        public static Scope CheckProperties(Scope paramTable, Scope s)
        {
            return null;
        }
        public override Value Interpret(Scope s)
        {
            VirtualFunctionType vft = new VirtualFunctionType(this, paramTable, s);
            s.putValue(this.funcName, vft);
            return vft;
        }
        public override Value Typecheck(Scope s)
        {
            Scope evaled = CheckProperties(paramTable, s);
            if (evaled == null)
            {
                evaled = new Scope();
                for (int i = 0; i < this.parameters.Count; i++)
                {
                    evaled.putType(parameters[i].value, Value.ANY);
                }
            }
            VirtualFunctionType vft = new VirtualFunctionType(this, evaled, s);
            s.putType(this.funcName, vft);
            return vft;
        }
        public override void Generate(CodeGenerator gene)
        {
            gene.Generate(this);
        }
        public override string ToString()
        {
            if (parameters.Count == 0)
            {
                return "(virtual function"+funcName+":void[] )";
            }
            return
                "(virtual function " +funcName +":["+
                parameters.Aggregate("", (init, p) => init + p.ToString()+" ") +
                "] )";
        }
    }
}
