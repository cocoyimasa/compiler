﻿using Compiler.CodeGen;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.Ast
{
    [DebuggerDisplay("{className}::{value}")]
    public class Field:Identifier
    {
        public Identifier className;
        public Scope fieldScope=new Scope();
        public Field()
        {
        }
        public Field(Token tok, Identifier _class, string val)
            :base(tok,val)
        {
            this.className = _class;
        }
        public override Value Interpret(Scope s)
        {
            Value val = className.Interpret(s);
            if(val is ClassClosure)
            {
                ClassClosure closure = (ClassClosure)val;
                Scope env = closure.properties;
                fieldScope = env;
                return base.Interpret(env);
            }
            return base.Interpret(s);
        }
        public override Value Typecheck(Scope s)
        {
            Value val = className.Typecheck(s);
            if(val is ClassType)
            {
                ClassType ct = (ClassType)val;
                Scope env = ct.properties;
                fieldScope = env;
                return base.Typecheck(env);
            }
            return base.Typecheck(s);
        }
        public override void Generate(CodeGenerator gene)
        {
            gene.Generate(this);
        }
        public override string ToString()
        {
            return className.value+"."+ value;
        }
    }
}
