﻿using Compiler.CodeGen;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.Ast
{
    [DebuggerDisplay("{name}{innerEnv}")]
    public class ClassStatement:Node
    {
        public string name;
        public List<string> parentClass;
        public Scope innerEnv;
        public List<Declaration> varDecls;
        public List<FunctionStatement> publicFuncs;
        public List<FunctionStatement> privateFuncs;
        public ClassStatement()
        {
        }
        public ClassStatement(
            Token tok,
            string name,
            List<Declaration> decls,
            List<FunctionStatement> publicFuncs,
            List<FunctionStatement> privateFuncs,
            Scope env)
            :base(tok)
        {
            this.name = name;
            this.parentClass = null;
            this.varDecls = decls;
            this.publicFuncs = publicFuncs;
            this.privateFuncs = privateFuncs;
        }
        public ClassStatement(
            Token tok,
            string name,
            List<string> parentClass,
            List<Declaration> decls,
            List<FunctionStatement> publicFuncs,
            List<FunctionStatement> privateFuncs,
            Scope env)
            : base(tok)
        {
            this.name = name;
            this.parentClass = parentClass;
            this.varDecls = decls;
            this.publicFuncs = publicFuncs;
            this.privateFuncs = privateFuncs;
        }
        public override Value Interpret(Scope s)
        {
            Scope evaled = new Scope(s);
            ClassClosure closure = new ClassClosure(this, evaled, s);
            s.putValue(name, closure);
            evaled = EvalValueScope(evaled);
            return closure;
        }
        public Scope EvalValueScope(Scope evaled)
        {
            foreach (var decl in varDecls)
            {
                //添加一个self变量
                Declaration self = new Declaration(Token.NewToken("self"), Identifier.NewIdentifier(name));
                Value selfVal = self.Interpret(evaled);
                evaled.Put(self.name, "value", selfVal);

                Value val = decl.Interpret(evaled);
                evaled.Put(decl.name, "value", val);
            }
            List<FunctionStatement> funcs = 
                publicFuncs.Concat(privateFuncs) as List<FunctionStatement>;
            foreach (var fun in funcs)
            {
                //如果是构造函数，为其block添加一句return self；
                if (name == fun.funcName)
                {
                    ((Block)fun.body).statements.Add
                        (
                        new ReturnStatement(
                            Token.NewToken("return"),
                            Identifier.NewIdentifier("self"))
                        );
                }
                Value val = fun.Interpret(evaled);
                if (val is Closure)
                {
                    Closure funcType = (Closure)val;
                    evaled.putValue(funcType.fun.funcName, (Closure)val);
                }
                else
                {
                    throw new CodeException(token, "解释错误！class内的函数" + fun.token.Value + "必须为Closure类型");
                }
            }
            return evaled;
        }
        public Scope EvalTypeScope(Scope evaled)
        {
            foreach (var decl in varDecls)
            {
                //添加一个self变量
                Declaration self = new Declaration(Token.NewToken("self"), Identifier.NewIdentifier(name));
                Value selfVal = self.Typecheck(evaled);
                evaled.Put(self.name, "type", selfVal);

                Value val = decl.Typecheck(evaled);
                evaled.Put(decl.name, "type", val);
            }
            List<FunctionStatement> funcs =
                publicFuncs.Concat(privateFuncs) as List<FunctionStatement>;
            foreach (var fun in funcs)
            {
                //如果是构造函数，为其block添加一句return self；
                if (name == fun.funcName)
                {
                    ((Block)fun.body).statements.Add
                        (
                        new ReturnStatement(
                            Token.NewToken("return"),
                            Identifier.NewIdentifier("self"))
                        );
                }
                Value val = fun.Typecheck(evaled);
                if (val is FunctionType)
                {
                    FunctionType funcType = (FunctionType)val;
                    evaled.putType(funcType.fun.funcName, (FunctionType)val);
                }
                else
                {
                    throw new CodeException(token, "类型错误！class内的函数" + fun.token.Value + "必须为FunctionType类型");
                }
            }
            return evaled;
        }
        public override Value Typecheck(Scope s)
        {
            Scope evaled = new Scope(s);
            evaled = EvalTypeScope(evaled);
            List<FunctionStatement> funcs =
                publicFuncs.Concat(privateFuncs) as List<FunctionStatement>;
            ClassType ct = new ClassType(varDecls, funcs, evaled, s);
            s.Put(name, "type", ct);
            return ct;
        }
        public override void Generate(CodeGenerator gene)
        {
            gene.Generate(this);
        }
        public override string ToString()
        {
            List<FunctionStatement> funcs =
                publicFuncs.Concat(privateFuncs) as List<FunctionStatement>;
            if (varDecls != null && funcs != null)
            {
                return
                    "( class:[" +
                    varDecls.Aggregate("", (init, v) => init + v.name + " ") +
                    "]( " +
                    funcs.Aggregate("", (init, v) => init + v.funcName + " ") + " ) )";
            }
            else if (varDecls == null && funcs != null)
            {
                return
                    "( class:[]( " +
                    funcs.Aggregate("", (init, v) => init + v.funcName + " ") + " ) )";
            }
            else if (varDecls != null && funcs == null)
            {
                return
                    "( class:[" +
                    varDecls.Aggregate("", (init, v) => init + v.name + " ") +
                    "]() )";
            }
            else
            {
                return "( class:[]() )";
            }
        }

    }
}
