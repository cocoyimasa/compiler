﻿using Compiler.CodeGen;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.Ast
{
    [DebuggerDisplay("{returnValue}{type}")]
    public class ReturnStatement : ControlTransferStatement
    {
        public Node returnValue;
        public Value type;
        public ReturnStatement()
        {

        }
        public ReturnStatement(Token tok, Node returnValue)
            : base(tok,ControlTransferType.Return_Statement,returnValue)
        {
            this.returnValue = returnValue;
        }
        public override Value Interpret(Scope s)
        {
            return returnValue.Interpret(s);
        }
        public override Value Typecheck(Scope s)
        {
            type = returnValue.Typecheck(s);
            return type;
        }
        public override void Generate(CodeGenerator gene)
        {
            gene.Generate(this);
        }
        public override string ToString()
        {
            return "return "+returnValue.ToString();
        }
    }
}
