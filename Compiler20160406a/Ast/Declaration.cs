﻿using Compiler.CodeGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.Ast
{
    public class Declaration:Node
    {
        public string name;
        public Identifier type;
        public Declaration()
        {

        }
        public Declaration(Token tok,Identifier _type)
            :base(tok)
        {
            this.name = tok.Value;
            this.type = _type;
        }
        public override Value Interpret(Scope s)
        {
            return type.Interpret(s);
        }
        public override Value Typecheck(Scope s)
        {
            return type.Typecheck(s);
        }
        public override void Generate(CodeGenerator gene)
        {
            gene.Generate(this);
        }
        public override string ToString()
        {
            return name + ":" + type.ToString();
        }
    }
}
