﻿using Compiler.CodeGen;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.Ast
{
    [DebuggerDisplay("{array}")]
    public class ArrayNode : Node
    {
        public ArrayNode parent;
        public List<Node> array = new List<Node>();
        public ArrayNode()
        {

        }
        public ArrayNode(Token tok, List<Node> array,ArrayNode parent)
            : base(tok)
        {
            this.array = array;
            this.parent = parent;
        }
        public ArrayNode(Token tok, ArrayNode parent)
            :base(tok)
        {
            this.parent = parent;
        }
        public override Value Interpret(Scope s)
        {
            return new ArrayType(InterpretList(array, s));
        }
        public override Value Typecheck(Scope s)
        {
            return new ArrayType(TypecheckList(array, s));
        }
        public override void Generate(CodeGenerator gene)
        {
            gene.Generate(this);
        }
        public override string ToString()
        {
            return array.Aggregate("", (i, a) => i + a.ToString());
        }
    }
}
