﻿using Compiler.CodeGen;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.Ast
{
    //关于new的处理，构造为MethodCall，className与Method Name相同，然后顺利复用MethodCall处理，大功告成
    [DebuggerDisplay("{objectName}:{func}:{arguments}")]
    public class MethodCall:FunctionCall
    {
        public Identifier objectName;//获取Scope
        public MethodCall()
        {

        }
        public MethodCall(Token tok, Identifier _class, Identifier func, Argument arguments)
            :base(tok,func,arguments)
        {
            this.objectName = _class;
        }
        public override Value Interpret(Scope s)
        {
            Value val = objectName.Interpret(s);
            if(val is ClassClosure)
            {
                ClassClosure closure = (ClassClosure)val;
                List<FunctionStatement> funcs = closure.classStmt.publicFuncs.Concat(closure.classStmt.privateFuncs) as List<FunctionStatement>;
                Scope classScope = new Scope(closure.properties);
                return base.Interpret(classScope);
            }
            return base.Interpret(s);
        }
        public override Value Typecheck(Scope s)
        {
            Value val = objectName.Typecheck(s);
            if(val is ClassType)
            {
                ClassType ct=(ClassType)val;
                Scope classScope = new Scope(ct.properties);
                return base.Typecheck(classScope);
            }
            else
            {
                throw new CodeException(objectName.token, "类名不存在");
            }
        }
        public override void Generate(CodeGenerator gene)
        {
            gene.Generate(this);
        }
        public override string ToString()
        {
            return objectName.value+"."+func.value + "( " + arguments.ToString() + " )";
        }
    }
}
