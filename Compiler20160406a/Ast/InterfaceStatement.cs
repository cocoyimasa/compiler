﻿using Compiler.CodeGen;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compiler.Ast
{
    [DebuggerDisplay("{name}{innerEnv}")]
    public class InterfaceStatement : Node
    {
        public string name;
        public List<string> parentInterfaces;
        public Scope innerEnv;
        //public List<Declaration> varDecls;
        public List<VirtualFunctionDeclaration> functions;

        public InterfaceStatement()
        {
        }
        public InterfaceStatement(
            Token tok,
            string name,
            //List<Declaration> decls,
            List<VirtualFunctionDeclaration> functions,
            Scope env)
            : base(tok)
        {
            this.name = name;
            this.parentInterfaces = null;
            //this.varDecls = decls;
            this.functions = functions;
        }
        public InterfaceStatement(
            Token tok,
            string name,
            List<string> parentClass,
            //List<Declaration> decls,
            List<VirtualFunctionDeclaration> functions,
            Scope env)
            : base(tok)
        {
            this.name = name;
            this.parentInterfaces = parentClass;
            //this.varDecls = decls;
            this.functions = functions;
        }
        public override Value Interpret(Scope s)
        {
            //Scope evaled = new Scope(s);
            //ClassClosure closure = new ClassClosure(this, evaled, s);
            //s.putValue(name, closure);
            //evaled = EvalValueScope(evaled);
            //return closure;
            return null;
        }
        public Scope EvalValueScope(Scope evaled)
        {
            //foreach (var decl in varDecls)
            //{
            //    //添加一个self变量
            //    Declaration self = new Declaration(Token.NewToken("self"), Identifier.NewIdentifier(name));
            //    Value selfVal = self.Interpret(evaled);
            //    evaled.Put(self.name, "value", selfVal);

            //    Value val = decl.Interpret(evaled);
            //    evaled.Put(decl.name, "value", val);
            //}
            foreach (var fun in functions)
            {
                //如果是构造函数，为其block添加一句return self；
                //if (name == fun.funcName)
                //{
                //    ((Block)fun.body).statements.Add
                //        (
                //        new ReturnStatement(
                //            Token.NewToken("return"),
                //            Identifier.NewIdentifier("self"))
                //        );
                //}
                Value val = fun.Interpret(evaled);
                if (val is Closure)
                {
                    Closure funcType = (Closure)val;
                    evaled.putValue(funcType.fun.funcName, (Closure)val);
                }
                else
                {
                    throw new CodeException(token, "解释错误！class内的函数" + fun.token.Value + "必须为Closure类型");
                }
            }
            return evaled;
        }
        public Scope EvalTypeScope(Scope evaled)
        {
            //foreach (var decl in varDecls)
            //{
            //    //添加一个self变量
            //    Declaration self = new Declaration(Token.NewToken("self"), Identifier.NewIdentifier(name));
            //    Value selfVal = self.Typecheck(evaled);
            //    evaled.Put(self.name, "type", selfVal);

            //    Value val = decl.Typecheck(evaled);
            //    evaled.Put(decl.name, "type", val);
            //}
            foreach (var fun in functions)
            {
                //如果是构造函数，为其block添加一句return self；
                //if (name == fun.funcName)
                //{
                //    ((Block)fun.body).statements.Add
                //        (
                //        new ReturnStatement(
                //            Token.NewToken("return"),
                //            Identifier.NewIdentifier("self"))
                //        );
                //}
                Value val = fun.Typecheck(evaled);
                if (val is FunctionType)
                {
                    FunctionType funcType = (FunctionType)val;
                    evaled.putType(funcType.fun.funcName, (FunctionType)val);
                }
                else
                {
                    throw new CodeException(token, "类型错误！class内的函数" + fun.token.Value + "必须为FunctionType类型");
                }
            }
            return evaled;
        }
        public override Value Typecheck(Scope s)
        {
            //Scope evaled = new Scope(s);
            //evaled = EvalTypeScope(evaled);
            //ClassType ct = new ClassType(varDecls, functions, evaled, s);
            //s.Put(name, "type", ct);
            //return ct;
            return null;
        }
        public override void Generate(CodeGenerator gene)
        {
            gene.Generate(this);
        }
        public override string ToString()
        {
            if (functions != null)
            {
                return
                    "( interface:[]( " +
                    functions.Aggregate("", (init, v) => init + v.funcName + " ") + " ) )";
            }
            else
            {
                return "( class:[]() )";
            }
        }

    }
}
